# SCIP log forwarder Heat template
Log forwarder HOT (Heat Orchestration Template). For use in Telenor's THC. To be deployed in an OpenStack environment.

The repository also includes a template for a HA setup. It needs a second set of IPs in parameters as well as a primary input VIP.

## Usage
The Heat template can be deployed directly in an OpenStack environment when supplied with the necessary parameters.

Minimum OpenStack requirements:
- CentOS >7.0 cloud image
- A flavor with at least 1 core, 2GB RAM and 15GB root disk
- Must be connected to 3 networks:
	- one for management
	- one for receiving syslog data
	- one for forwarding log data to Telenor's backend

### Parameters
The stack is configured through parameters. It is recommended to create a YAML environment file and use it during stack creation with the `-e <file>` option. Example structure (`env.yml`):
```yaml
parameters:
  image: CentOS-7-x86_64-GenericCloud-1809
  flavor: appdev.4-8
  zone: nova
  ...
```

All OpenStack resource parameters (networks, image, flavor, etc.) can be given in name or UUID form. Names will not work if multiple resources have the same name.

There is no parameter for instance name. The instance(s) will be named after the stack name.

#### Network parameters
| Parameter | Description | Required |
|-----------|-------------|----------|
| `net_log_input` | Network for receiving syslog input | Yes |
| `net_log_input_subnet` | Subnet in the log input network | Yes |
| `net_log_input_ip` | Fixed IP to receive log input | Yes |
| `net_log_output` | Network for sending aggregated logs over AMQP/TLS to TnDK OSS | Yes |
| `net_log_output_subnet` | Subnet in the log output network | Yes |
| `net_log_output_ip` | Fixed IP to send log output | Yes |
| `net_mgmt` | Network allowing remote management connections | Yes |
| `net_mgmt_subnet` | Subnet in the management network | Yes |
| `net_mgmt_ip` | Fixed IP for management | Yes |

#### VM parameters
| Parameter | Description | Required |
|-----------|-------------|----------|
| `image` | Glance image to use for instance(s) | Yes |
| `flavor` | Flavour to use for instance(s) | Yes |
| `vm_domain` | Latter part of FQDN (`<stack_name>.<vm_domain>`) | Yes |
| `zone_vm` | Availability zone to place all resources in | Yes |
| `zone_volume` | Availability zone to place all resources in | Yes |
| `data_volume_size` | Size of data volume (in GB) | No |
| `data_volume_type` | Type of volume (default rdb) | No |

## Technical details
### Networking
Cloud-init network configuration is disabled. The network interfaces on the VMs are configured "manually" through network scripts in `/etc/sysconfig/network-scripts`.
They use CentOS' `network` service to set static configurations on each device, based on the settings of each virtual OpenStack interface.

We chose to do this to avoid having to feed metadata to the instances.

The input interface uses a policy-based routing table (`logfwd`). This is so we can have the default route on the OAM interface.

### Access
Public keys are appended to `/root/.ssh/authorized_keys` on instance creation to allow remote SSH management (using Ansible) of the instance(s).
Telenor OSS and Nokia has the matching private keys.
